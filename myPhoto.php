<?php

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }


//var_dump($_FILES);

$name = "";
$nameErr = "";
$priv_or_pub = "";
$priv_or_pubErr = "";


// $selected_radio = $_POST['priv_or_pub'];
$date = time();

// random try #5
// if ($selected_radio == 'public') {
//     $public_status = 'checked'; 
// }
//     else if ($selected_radio == 'private') {
//     $private_status = 'checked';
//     }
//     else {
//         $priv_or_pubErr = "You must select one";
//     }

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // check if the field is empty - if it is - print the error
    if (empty($_POST["name"])) {
      $nameErr = "Name is required";
    } else {
      $name = test_input($_POST["name"]);
      // check if name only contains letters and whitespace
      if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
        $nameErr = "Only letters and white space allowed"; 
      }
      // check if name contains over 2 characters
      if(strlen($name) < 3) {
        $nameErr = "Length of name must be over 2 characters";
      }
    } 
    //check if one of the radio buttons is selected
    if (empty($_POST["priv_or_pub"])) {
        $priv_or_pubErr = "You must select one";
    }
}




if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {

    $file_name = $_FILES['file']["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"];
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES['file']["error"];
   
   if ($file_error >0) {
       echo "Something went wrong during file upload!";
   }
   else {
       echo exif_imagetype($file_temp)."<br />";
       if (!exif_imagetype($file_temp)) {
         exit("File is not a picture!");
       }
       if(exif_imagetype($file_temp) != IMAGETYPE_JPEG){
        exit ("Not a JPEG image");   
       }
       
       
       $ext_temp = explode(".", $file_name);
       $extension = end($ext_temp);
       
       $new_file_name = "$name" . "-" . "$date" . "-" . rand(1,10) . ".$extension";
       $file_name_name = 'slika';

       $directory = "private";
       $directory2 = "public";

       $private_status = 'unchecked';
       $public_status = 'unchecked';
       
       $upload = "$directory/$new_file_name"; 
       $upload2 = "$directory2/$new_file_name"; 
    
       // upload fajla

       if (isset($_POST['priv_or_pub'])) {
        $selected_radio = $_POST['priv_or_pub'];
        if ($selected_radio == 'private') {
            $private_status = 'checked';  
            }
           elseif ($selected_radio == 'public') { 
            $public_status = 'checked';
            }   
       }
       
       if (!is_dir($directory) AND $private_status == 'checked' AND $nameErr == "") {
         mkdir($directory);
       }
       elseif (!is_dir($directory2) AND $public_status == 'checked' AND $nameErr == "") {
            mkdir($directory2);
       }
       
       if (!file_exists($upload) AND $private_status == 'checked') { 
           if (move_uploaded_file($file_temp, $upload)) {
               $size = getimagesize($upload);
               //var_dump($size);
               foreach ($size as $key => $value)
               echo "$key = $value<br />";
               echo "<img src=\"$upload\" $size[3] border=\"0\" alt=\"$file_name\" />";
           }
           else {
             echo "<p><b>Error!</b></p>";
           }    
       }

       if (!file_exists($upload2) AND $public_status == 'checked') { 
        if (move_uploaded_file($file_temp, $upload2)) {
            $size = getimagesize($upload2);
            //var_dump($size);
            foreach ($size as $key => $value)
            echo "$key = $value<br />";
            echo "<img src=\"$upload2\" $size[3] border=\"0\" alt=\"$file_name\" />";
        }
        else {
          echo "<p><b>Error!</b></p>";
        }    
    }
}
}

if (!empty($nameErr) or !empty($priv_or_pubErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&priv_or_pub=" . urlencode($_POST["priv_or_pub"]);
    $params .= "&priv_or_pubErr=" . urlencode($priv_or_pubErr);
    header("Location: index.php?" . $params);
}

?>