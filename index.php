<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="https://necolas.github.io/normalize.css/8.0.0/normalize.css">
<title>PHP</title>
</head>
<body>
<?php

$name = "";
$nameErr = "";
$priv_or_pub = "";
$priv_or_pubErr = "";

if (isset($_GET['name'])) { $name = $_GET['name']; }
if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }

if (isset($_GET['priv_or_pub'])) { $priv_or_pub = $_GET['priv_or_pub']; }
if (isset($_GET['priv_or_pubErr'])) { $priv_or_pubErr = $_GET['priv_or_pubErr']; }

include("header.html");
include("menu.html");

if(!isset($_GET["link"]))
    $_GET["link"]="photo";

switch ($_GET["link"])
{
    case "index";
    include("index.html");
    break;

    case "photo";
    include("photo.html");
    break;

    case "task";
    include("task.html");
    break;

    default:
    include("index.html");
    break;
}

include("footer.html");

?>

</body>
</html>